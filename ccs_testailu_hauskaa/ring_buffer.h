/*
 * ring_buffer.h
 *
 *  Created on: 28.3.2016
 *      Author: Joonas
 */

#ifndef RING_BUFFER_H_
#define RING_BUFFER_H_

#include <stddef.h>
#include <stdint.h>

/* Define a type for ring buffer attributes element size, element number and ptr to buffer */
typedef struct {
	size_t s_elem;
	size_t n_elem;
	void *buffer;
} rb_attr_t;

/* Ring buffer descriptor, used by caller to access the ring buffer initialized.
 * Used as an index into an array of the ring buffer.	*/
typedef unsigned int rbd_t;

/* Ring buffer initialization */
int ring_buffer_init(rbd_t *rbd, rb_attr_t *attr);

/* Puts data into the ring buffer, uses some cool math for the offset*/
int ring_buffer_put(rbd_t rbd, const void *data);

/* Gets data from ring buffer, important to have tail increment AFTER, so interrupts wont put and
 * overwrite data before get can get it */
int ring_buffer_get(rbd_t rbd, void *data);




#endif /* RING_BUFFER_H_ */
