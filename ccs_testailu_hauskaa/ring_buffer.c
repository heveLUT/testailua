/*
 * ring_buffer.c
 *
 *  Created on: 28.3.2016
 *      Author: Joonas
 *
 *      Buffer for Rx data from UART
 *      Allows any kind of data -> Need to know number and size of elements in buffer
 *      When buffers size is power of 2 incrementing the index enough will couse overflow and
 *      reset back to the start of the buffer creating a loop. Since the buffer isnt wrapped
 *      around difference between head and tail will never be 0 if there is data in the buffer.
 *      So buffer is empty only when difference head-tail == n_elem.
 *      Using power of 2 for size most important part comes in allowing to get modulus with logic
 *      AND operation, so we dont have to use more expensive % operation.
 */

#include "ring_buffer.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

// Maximum number of ring buffers
#define RING_BUFFER_MAX 1




/*	Ring buffer structure	*/
struct ring_buffer {
	size_t s_elem;
	size_t n_elem;
	uint8_t *buf;
	volatile size_t head;		// Accessed from multiple context so
	volatile size_t tail;		// has to be volatile
};

/* Allocate array of ring buffers */
static struct ring_buffer _rb[RING_BUFFER_MAX];
static int _ring_buffer_full(struct ring_buffer *rb);
static int _ring_buffer_empty(struct ring_buffer *rb);

/* Ring buffer initialization */
int ring_buffer_init(rbd_t *rbd, rb_attr_t *attr) {
	static int index = 0;
	int err = -1;

	/* Check if max n of buffers is not yet initialized and ptrs exist */
	if((index < RING_BUFFER_MAX) && (rbd != NULL) && (attr != NULL)) {
		/* Check if ptr to buffer in buffer attributes exist and size of elements > 0 */
		if((attr->buffer != NULL) && (attr->s_elem > 0)) {
			/* Check that the size of the ring buffer is a power of 2 */
			if(((attr->n_elem - 1) & attr->n_elem) == 0) {
				/* Initialize the ring buffer internal variables */
				_rb[index].head = 0;
				_rb[index].tail = 0;
				_rb[index].buf = attr->buffer;
				_rb[index].s_elem = attr->s_elem;
				_rb[index].n_elem = attr->n_elem;

				*rbd = index++;
				err = 0;
			}
		}
	}
	return err;
}

/* Puts data into the ring buffer, uses some cool math for the offset*/
int ring_buffer_put(rbd_t rbd, const void *data) {
	int err = 0;

	/* Check that given index is not over and buffer isnt full */
	if((rbd < RING_BUFFER_MAX) && (_ring_buffer_full(&_rb[rbd]) == 0)) {
		const size_t offset = (_rb[rbd].head & (_rb[rbd].n_elem - 1)) * _rb[rbd].s_elem;
		memcpy(&(_rb[rbd].buf[offset]), data, _rb[rbd].s_elem);
		_rb[rbd].head++;
	} else {
		err = -1;
	}
	return err;
}

/* Gets data from ring buffer, important to have tail increment AFTER, so interrupts wont put and
 * overwrite data before get can get it */
int ring_buffer_get(rbd_t rbd, void *data) {
	int err = 0;
	if((rbd < RING_BUFFER_MAX) && (_ring_buffer_empty(&_rb[rbd]) == 0)) {
		const size_t offset = (_rb[rbd].tail & (_rb[rbd].n_elem - 1)) * _rb[rbd].s_elem;
		memcpy(data, &(_rb[rbd].buf[offset]), _rb[rbd].s_elem);
		_rb[rbd].tail++;
	} else {
		err = -1;
	}
	return err;
}

/* Helper functions to check if buffer is full or empty */
static int _ring_buffer_full(struct ring_buffer *rb) {
	return((rb->head - rb->tail) == rb->n_elem) ? 1 : 0;
}

static int _ring_buffer_empty(struct ring_buffer *rb) {
	return((rb->head - rb->tail) == 0U) ? 1 : 0;
}
