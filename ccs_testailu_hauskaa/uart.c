/*
 * uart.c
 *
 * Tarkotus oli koittaa oppia jotain ja ymmärtää mutta päätyikin häpeilemättömäksi kopionniksi ohjeista
 * http://www.simplyembedded.org/tutorials/msp430-uart/
 */

#include "uart.h"
#include "ring_buffer.h"
#include <stdint.h>
#include <stddef.h>
#include <msp430.h>

#define ARRAY_SIZE(array) (sizeof(array)/sizeof(array[0]))

/* RX ring buffer */
static rbd_t _rbd;
static char _rbmem[8];	// UART 8 bit mode so char is sufficient if 9 or 10 bit then would have to use uint16_t
// The array size of 8 might have to be made larger for longer commands

int uart_init(void) {
	int status = -1;

	// Done when USCI logic in reset state
	if(UCA0CTL1 & UCSWRST) {

		// Clock source to SMCLK
		UCA0CTL1 |= UCSSEL_2;

		rb_attr_t attr = {sizeof(_rbmem[0]), ARRAY_SIZE(_rbmem), _rbmem};

		// Set the baud rate to 9600
		UCA0BR0 = 104;
		UCA0BR1 = 0;
		UCA0MCTL = UCBRS0;

		/* Init the ring buffer */
		if(ring_buffer_init(&_rbd, &attr) == 0) {
			// Enable USCI - out of reset
			UCA0CTL1 &= ~UCSWRST;
			// Enable rx interrupts
			IE2 |= UCA0RXIE;
			status = 0;
		}
	}
	return status;
}	// uart_init

int uart_getchar(void) {
	char chr = -1;
	ring_buffer_get(_rbd, &chr);
	return chr;
}	// uart_getchar

int uart_putchar(int c) {
	while(!(IFG2 & UCA0TXIFG));
	UCA0TXBUF = (char)c;
	return 0;
}	// uart_putchar

int uart_puts(const char *str) {
	int status = -1;

	if(str != NULL) {
		status = 0;

		while(*str != '\0') {			// Read chars until NULL
			while(!(IFG2 & UCA0TXIFG));	// Wait for tx buffer to be ready
			UCA0TXBUF = *str;			// Transmit data
			if(*str == '\n') {
				while(!(IFG2 & UCA0TXIFG));
				UCA0TXBUF = '\r';		// Add carriage return so shit works when only new line is sent
			}
			str++;
		}
	}
	return status;
}	// uart_puts



#pragma vector = USCIAB0RX_VECTOR	// Interrupt to get received data
__interrupt void RX_ISR(void) {
	// Check for USCI A0 interrupt ( same vector for both A0 and B0 )
	if(IFG2 & UCA0RXIFG) {
		const char c = UCA0RXBUF;
		// Clear the interrupt flag
		IFG2 &= ~UCA0RXIFG;
		ring_buffer_put(_rbd, &c);
	}
}	// RX_ISR






