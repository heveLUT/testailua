################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430g2553.cmd 

C_SRCS += \
../blink.c \
../ring_buffer.c \
../uart.c 

OBJS += \
./blink.obj \
./ring_buffer.obj \
./uart.obj 

C_DEPS += \
./blink.pp \
./ring_buffer.pp \
./uart.pp 

C_DEPS__QUOTED += \
"blink.pp" \
"ring_buffer.pp" \
"uart.pp" 

OBJS__QUOTED += \
"blink.obj" \
"ring_buffer.obj" \
"uart.obj" 

C_SRCS__QUOTED += \
"../blink.c" \
"../ring_buffer.c" \
"../uart.c" 


