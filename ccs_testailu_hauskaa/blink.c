/*
 * Blink blink hauskaa
 * Tuunailtuna napin painallus pausella
 * L�hinn� CCS testailua varten ja pinnien/rekistereiden hallintaa ilman
 * arduino/energia systeemej� (wiring?)
 * Energia alkoi kaikin puolin vituttamaan.
 *
 * Interruptien testailua. Vaihda vilkkuvaa ledia buttonilla/Timer_A avulla.
 * Mahdollisesti hyv� tapa toteuttaa s��dett�v� sykli hengitykselle.
 * Tallentaminen flash muistiin ja sen lukeminen toimii halutulla tavalla.
 *
 * Runsaasti kommentoitu. Koodin toiminnan yl�s kirjoittaminen auttaa omaa oppimista
 *
 * */

// Change to another if using something other than MSP430G2553 chip
// msp430.h can be used aswell
#include <msp430g2553.h>
#include <inttypes.h>
#include "uart.h"
#include "ring_buffer.h"

#define pinRLED 	BIT0
#define pinGLED 	BIT6
#define pinBTN 		BIT3
#define RXD			BIT1
#define TXD			BIT2

/* 	FLASH MEMORY SPACES FOR SAVED SETTINGS	*/			// SegB starts at 0x1080, lenght 0x0040, end 0x10BF (?)
#define BSTARTADR			(int *) 0x1080
#define INTERRUPT_CYCLES	*(uint16_t *) 0x1080		// int type ptr so takes 1 byte
#define DELAY_CYCLES		*(uint32_t *) 0x1082		// 4 bytes after INTERRUPT_CYCLES, 0x1086 next free one

/*  FUNCTION DECLARATIONS  */
void delay(volatile uint32_t i);				// Doesn't work without volatile
char wrFlash(uint16_t iCyc, uint32_t dCyc);		// Saves dCyc with least sig. word(2bytes) first
void test_msg(void);

/*	GLOBAL VARIABLES	*/
// Variables used by ISR, might be better if they werent global but its too tempting to do it the easy way.
uint8_t state = 0;
uint8_t uiCounter = 0; 			// Char instead of int, 1 byte instead of 2
uint8_t maxTimerCount = 20;		// Default = 20, read possible saved value from memory in main()



int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	DCOCTL = 0;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;			// Calibrate to 1Mhz clock

	P1OUT = 0;						// Reset all outs to 0
	P1DIR = pinRLED + pinGLED;			// Set P1.0 and P1.6 to output direction
	P1DIR &= ~pinBTN;				// Set P1.3 as input  ->  sets bit 3 to 0 and others unchanged
	//P1OUT |= pinRLED;					// Set P1.0 to 1

	P1REN |= pinBTN;				// Set resistor for pinBTN
	P1OUT |= pinBTN;				// Specifies use resistor as pull-up

	// Enable interrupt for P1.3
	// PxIES selects the interrupt edge
	P1IES |= pinBTN;				// interrupt on high -> low edge selected with IES.x = 1
	P1IFG &= ~pinBTN;				// clear interrupt flag register before enabling
	P1IE |= pinBTN;					//  Enable interrupt for BTN

	// Config RX and TX pins for UART
	P1SEL |= RXD + TXD;				// 0110 so P1.1 and p1.2 will be set for secondary peripheral function
	P1SEL2 |= RXD + TXD;			// These pins cannot generate interrupts

	// Timer_A config for interrupting
	TACCR0 = 62500 - 1;							// Timer reset value
	TACCTL0 = CCIE;								// Enable interrupts for CCR0;
	TACTL = TASSEL_2 + ID_3 + MC_1 + TACLR;		// clk choice(SMCLK), div8, up-mode, clear


	// Enables maskable interrupts by changing status register General Interrupt Enable
	// __BIS_SR(GIE);	Can be used to change other status register bits with same instruction
	__enable_interrupt();		 // easier to read version

	int uartStatus;
	uartStatus = uart_init();
	if(uartStatus == -1) {		// Maybe some kind of error handling is needed
	} else
		uart_puts("Hello\n");



	uint32_t uiDelay = 50000; 	// Get from memory
	char wrOk;
	int inChar;

	/*	CHECK MEMORY FOR NONDEFAULT SETTINGS	*/
	if(DELAY_CYCLES != 0xFFFFFFFF) {
		uiDelay = DELAY_CYCLES;
	}
	if(INTERRUPT_CYCLES != 0xFFFF) {
		maxTimerCount = INTERRUPT_CYCLES;
	}

	//ptrData = formatWrData(maxTimerCount, uiDelay);
	wrOk = wrFlash(maxTimerCount, uiDelay);

	if(wrOk == 1) {
		delay(50);	// Send okwrite message back
	}


	for(;;) {

		inChar = uart_getchar();
		if(inChar != 0xFF) {
			uart_putchar(inChar);
		}

/*
		switch(state) {
			case 1:
				P1OUT ^= pinRLED;		// Toggle P1.0 and P1.6 using exclusive-OR
				delay(uiDelay);				// Delay by number of loops
				break;
			case 0:
				P1OUT ^= pinGLED;		// Toggle P1.0 and P1.6 using exclusive-OR
				delay(uiDelay);				// Delay by number of loops
				break;
			default:
				break;
		} */
	}
	
	return 0;
}	// main


void delay(volatile uint32_t i) {
	do i--;			// Loop until i == 0, compiler propably optimizes something and makes this not work without
	while(i != 0);	// volatile
} 	// delay


char wrFlash(uint16_t iCyc, uint32_t dCyc) {
	volatile int i;

	FCTL2 = FWKEY + FSSEL_1 + FN1; 	// use MCLK/3
	FCTL1 = FWKEY + ERASE; 			// set to erase flash segment
	FCTL3 = FWKEY;					// set LOCK to 0
	INTERRUPT_CYCLES = 0x00;		// dummy write to iCyc to init erasing seg
	while(FCTL3 & BUSY);			// Wait until dont erasing
	FCTL1 = FWKEY + WRT;			// Set write bit
	INTERRUPT_CYCLES = iCyc;		// Write iCyc to flash
	while(FCTL3 & BUSY);
	DELAY_CYCLES = dCyc;
	while(FCTL3 & BUSY);

	FCTL1 = FWKEY;					// Clear write bit
	FCTL3 = FWKEY + LOCK;			// set lock bit

	return 1;		// Check if write ok, send message back!!
}	// wrFlash





/*	INTERRUPT SERVICE ROUTINES	*/

#pragma vector = TIMER0_A0_VECTOR
__interrupt void CCR0_ISR(void) {

	if(uiCounter++ == maxTimerCount) {		// count to 20 is about 10s with 1Mhz clock and /8 divider in TACTL
								// ISR goes off every .5seconds, with low frq crystal this could be much longer
		state ^= 1;			// Toggle state
		P1OUT &= ~pinRLED + pinGLED;	// turn leds off
		uiCounter = 0;
	}
}	// CCR0_ISR


#pragma vector = PORT1_VECTOR		// INTERRUPT FLAG for BTN P1IFG.3
__interrupt void P1_ISR(void) {

	// switch to handle P1 bits interrupts
	switch(P1IFG & pinBTN) {
		case pinBTN:			// Interrupt of P1.3
			P1IFG &= ~pinBTN;  	// Clear the interrupt flag
			state ^= 1;			// Toggle state
			P1OUT &= ~pinRLED + pinGLED;	// turn leds off
			return;

		default:
			P1IFG = 0;		// Might not be needed but resets interrupt flags in P1
							// since we only care about P1.3
			return;
	}

}	// P1_ISR











